package net.site40.rodit.tinyrpg.game.chat;

public interface IChatSender {

	public boolean showName();
	public String getDisplayName();
}
